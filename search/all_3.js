var searchData=
[
  ['cell_5fbal_13',['cell_bal',['../db/da8/classBQ76930.html#a73e83cfe09b129e6a46dc8f7742baee9',1,'BQ76930::cell_bal(void)'],['../db/da8/classBQ76930.html#a561e5de1dc40e7e42f86b2c82431e930',1,'BQ76930::cell_bal(uint16_t cells)'],['../d3/dfc/classBQ76940.html#a797edf3d6df532fa44c1cca327a735f6',1,'BQ76940::cell_bal(void)'],['../d3/dfc/classBQ76940.html#a276ae7236e262b8eeff41cfc6b0d6393',1,'BQ76940::cell_bal(uint16_t cells)'],['../d5/d11/classBQ769xx.html#a31f8c47a0b227fd0deed2434081e46d8',1,'BQ769xx::cell_bal(void)=0'],['../d5/d11/classBQ769xx.html#aad62b1d8cd02eac9f131f2225810ca9f',1,'BQ769xx::cell_bal(uint16_t cells)=0']]],
  ['cell_5fbal1_14',['cell_bal1',['../d5/d11/classBQ769xx.html#af4fd2605e6ade4518ab464296632f52c',1,'BQ769xx::cell_bal1(uint8_t cells)'],['../d5/d11/classBQ769xx.html#afa71ab4d7d528898c657de9316e79192',1,'BQ769xx::cell_bal1(void)']]],
  ['cell_5fbal2_15',['cell_bal2',['../d5/d11/classBQ769xx.html#a33f28cdba5688836ab2e6b30b8ca2a61',1,'BQ769xx::cell_bal2(uint8_t cells)'],['../d5/d11/classBQ769xx.html#aeedb9a5f624b4601cd85f3c7e958db6a',1,'BQ769xx::cell_bal2(void)']]],
  ['cell_5fbal3_16',['cell_bal3',['../d5/d11/classBQ769xx.html#aa008cc0c5d9a94295659d55d98fe4f32',1,'BQ769xx::cell_bal3(void)'],['../d5/d11/classBQ769xx.html#a2f00c7bd39e422d4cb0143248b436b15',1,'BQ769xx::cell_bal3(uint8_t cells)']]],
  ['cell_5fbalancing_5fcheck_17',['cell_balancing_check',['../d6/d17/BMS__utils_8h.html#adc4aa15fc2787877083c2f612b17373d',1,'BMS']]],
  ['cell_5fvoltages_18',['cell_voltages',['../db/da8/classBQ76930.html#ae0a813ce93e7e83315ef28d7c8c608be',1,'BQ76930::cell_voltages()'],['../d3/dfc/classBQ76940.html#a905d86aad78ddb64390fbd2afef9b69f',1,'BQ76940::cell_voltages()'],['../d5/d11/classBQ769xx.html#acb79a7c2f55a931640ab1195f29f7328',1,'BQ769xx::cell_voltages()']]],
  ['cmsis_19',['CMSIS',['../dd/d3b/group__CMSIS.html',1,'']]],
  ['crc_2ec_20',['crc.c',['../d8/d13/crc_8c.html',1,'']]],
  ['crc_2eh_21',['crc.h',['../dc/de6/crc_8h.html',1,'']]],
  ['create_5fpacket_5fw_5fcrc_22',['create_packet_w_crc',['../d5/d11/classBQ769xx.html#ae54b6a84c08f8594f85235fe3154b535',1,'BQ769xx']]]
];
