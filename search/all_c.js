var searchData=
[
  ['read_5fbat_5fadc_55',['read_bat_ADC',['../d5/d11/classBQ769xx.html#ab11faf3b9a32a6a8a459182710f43c4c',1,'BQ769xx']]],
  ['read_5fcell_5fadc_56',['read_cell_ADC',['../db/da8/classBQ76930.html#a706afc6ca236e8d9bf8099372b1083a2',1,'BQ76930::read_cell_ADC()'],['../d3/dfc/classBQ76940.html#afaabb096bd47ffe0612eba20a890a323',1,'BQ76940::read_cell_ADC()'],['../d5/d11/classBQ769xx.html#a4f97f8537375364618ab7ea950ba45a3',1,'BQ769xx::read_cell_ADC()']]],
  ['read_5fcell_5fvoltages_57',['read_cell_voltages',['../d6/d17/BMS__utils_8h.html#a1c77b2ecba47625960e8a2bb352cc065',1,'BMS']]],
  ['read_5fn_5fupdate_5fadc_5fgain_58',['read_n_update_ADC_gain',['../d5/d11/classBQ769xx.html#a85d3ffea95a8eb99590fd3931e486d3b',1,'BQ769xx']]],
  ['read_5fn_5fupdate_5fadc_5foffset_59',['read_n_update_ADC_offset',['../d5/d11/classBQ769xx.html#a1d786f73353c7a185db2d3d28b29231e',1,'BQ769xx']]],
  ['register_5fread_5fw_5fcrc_60',['register_read_w_crc',['../d5/d11/classBQ769xx.html#acc57517062f23284df9c4e79ca656581',1,'BQ769xx']]],
  ['register_5fwrite_5fw_5fcrc_61',['register_write_w_crc',['../d5/d11/classBQ769xx.html#ac4f054178b98c38aa355bef101dc1811',1,'BQ769xx']]]
];
