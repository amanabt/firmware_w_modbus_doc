var searchData=
[
  ['bat_5fvoltage_4',['bat_voltage',['../db/da8/classBQ76930.html#aef1f4a10cf9419a138b11e1d5e67d1b8',1,'BQ76930::bat_voltage()'],['../d3/dfc/classBQ76940.html#ae375da48c5b59662fc9fe2e444df3196',1,'BQ76940::bat_voltage()'],['../d5/d11/classBQ769xx.html#ad24283f272ae3bd7ebd6fb09693cf3d6',1,'BQ769xx::bat_voltage()']]],
  ['bms_5futils_2eh_5',['BMS_utils.h',['../d6/d17/BMS__utils_8h.html',1,'']]],
  ['boot_6',['boot',['../db/da8/classBQ76930.html#af2bfc292e187ba13cd900989ca249a24',1,'BQ76930::boot()'],['../d3/dfc/classBQ76940.html#a80f6888a121176daa404d9a9cf38f4b0',1,'BQ76940::boot()'],['../d5/d11/classBQ769xx.html#a1a00d3c3bd3f6a468172954bf7ea91c4',1,'BQ769xx::boot()']]],
  ['bq76930_7',['BQ76930',['../db/da8/classBQ76930.html',1,'']]],
  ['bq76930_2eh_8',['BQ76930.h',['../d8/d66/BQ76930_8h.html',1,'']]],
  ['bq76940_9',['BQ76940',['../d3/dfc/classBQ76940.html',1,'']]],
  ['bq76940_2eh_10',['BQ76940.h',['../d2/d28/BQ76940_8h.html',1,'']]],
  ['bq769xx_11',['BQ769xx',['../d5/d11/classBQ769xx.html',1,'']]],
  ['bq769xx_2eh_12',['BQ769xx.h',['../d0/d51/BQ769xx_8h.html',1,'']]]
];
